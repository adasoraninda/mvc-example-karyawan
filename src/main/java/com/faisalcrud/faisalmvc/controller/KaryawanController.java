package com.faisalcrud.faisalmvc.controller;

import com.faisalcrud.faisalmvc.model.reponse.KaryawanResponse;
import com.faisalcrud.faisalmvc.model.request.KaryawanRequest;
import com.faisalcrud.faisalmvc.service.KaryawanService;
import lombok.AllArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@RestController
@AllArgsConstructor
@RequestMapping(path = "karyawan")
public class KaryawanController {

    private final KaryawanService service;

    @PostMapping(
            consumes = {
                    MediaType.APPLICATION_JSON_VALUE,
                    MediaType.APPLICATION_XML_VALUE},
            produces = {
                    MediaType.APPLICATION_JSON_VALUE,
                    MediaType.APPLICATION_XML_VALUE})
    public KaryawanResponse createKaryawan(
            @RequestBody KaryawanRequest karyawanRequest
    ) {
        return service.createKaryawan(karyawanRequest);
    }

    @PostMapping(
            path = "/banyak",
            produces = {
                    MediaType.APPLICATION_JSON_VALUE,
                    MediaType.APPLICATION_XML_VALUE})
    public List<KaryawanResponse> createBanyakKaryawan(
            @RequestBody Map<String, List<KaryawanRequest>> karyawanRequest
    ) {
        return service.createBanyakKaryawan(karyawanRequest.get("employees"));
    }

    @GetMapping
    public List<KaryawanResponse> readBanyakKaryawan() {
        return service.readBanyaKaryawan();
    }

    @GetMapping(path = "/{id}")
    public KaryawanResponse readKaryawanById(
            @PathVariable(value = "id") Long id
    ) {
        return service.readByKaryawanId(id);
    }

    @PutMapping(path = "/{id}")
    public KaryawanResponse updateKaryawanById(
            @PathVariable(value = "id") Long id,
            @RequestBody KaryawanRequest karyawanRequest
    ) {
        return service.updateKaryawanById(id, karyawanRequest);
    }

    @DeleteMapping(path = "/{id}")
    public void deleteKaryawanById(@PathVariable(value = "id") Long id) {
        service.deleteKaryawanById(id);
    }

    @DeleteMapping
    public void deleteBanyakKaryawan() {
        service.deleteBanyaKaryawan();
    }

}
