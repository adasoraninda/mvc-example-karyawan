package com.faisalcrud.faisalmvc.model.request;

import lombok.Data;

@Data
public class KaryawanRequest {
    private String name;
}
