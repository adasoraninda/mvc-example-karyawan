package com.faisalcrud.faisalmvc.model.reponse;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class KaryawanResponse {
    private Long id;
    private String name;
}
