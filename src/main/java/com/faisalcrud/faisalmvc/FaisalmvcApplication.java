package com.faisalcrud.faisalmvc;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FaisalmvcApplication {

	public static void main(String[] args) {
		SpringApplication.run(FaisalmvcApplication.class, args);
	}

}
