package com.faisalcrud.faisalmvc.service;

import com.faisalcrud.faisalmvc.model.entity.Karyawan;
import com.faisalcrud.faisalmvc.model.reponse.KaryawanResponse;
import com.faisalcrud.faisalmvc.model.request.KaryawanRequest;
import com.faisalcrud.faisalmvc.repository.KaryawanRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
public class KaryawanService {

    private final KaryawanRepository repository;

    // CRUD CREATE READ UPDATE DELETE
    // createKaryawan(request), trus convert key entity karena repository.save
    // hanya nerima parameternya bertipe entity.
    // repository.save balikin data bertipe entity.
    // karena fungsi createKaryawan ngebalikin data bertipe response,
    // maka convert hasil dari repository.save key response.
    public KaryawanResponse createKaryawan(KaryawanRequest karyawanRequest) {
        var karyawan = new Karyawan(null, karyawanRequest.getName());
        var savedKaryawan = repository.save(karyawan);

        return new KaryawanResponse(
                savedKaryawan.getId(),
                savedKaryawan.getName());
    }

    public List<KaryawanResponse> createBanyakKaryawan(List<KaryawanRequest> banyakKaryawanReq) {
        var banyakKaryawan = banyakKaryawanReq.stream()
                .map(k -> new Karyawan(null, k.getName()))
                .collect(Collectors.toList());

        return repository.saveAll(banyakKaryawan).stream()
                .map(k -> new KaryawanResponse(k.getId(), k.getName()))
                .collect(Collectors.toList());

    }

    public List<KaryawanResponse> readBanyaKaryawan() {
        return repository.findAll().stream()
                .map(k -> new KaryawanResponse(k.getId(), k.getName()))
                .collect(Collectors.toList());
    }

    public KaryawanResponse readByKaryawanId(Long id) {
        return repository.findById(id)
                .map(k -> new KaryawanResponse(k.getId(), k.getName()))
                .orElseThrow(NullPointerException::new);
    }

    public KaryawanResponse updateKaryawanById(Long id, KaryawanRequest newKaryawan) {
        var isKaryawanExists = repository.existsById(id);

        if (!isKaryawanExists) {
            throw new IllegalArgumentException();
        }

        var karyawan = new Karyawan(null, newKaryawan.getName());
        karyawan.setId(id);

        var savedKaryawan = repository.save(karyawan);

        return new KaryawanResponse(savedKaryawan.getId(), savedKaryawan.getName());
    }

    public void deleteKaryawanById(Long id) {
        var isKaryawanExists = repository.existsById(id);

        if (!isKaryawanExists) {
            throw new IllegalArgumentException();
        }

        repository.delete(repository.getById(id));
    }

    public void deleteBanyaKaryawan() {
        repository.deleteAll();
    }

}
