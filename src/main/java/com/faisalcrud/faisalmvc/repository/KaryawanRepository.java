package com.faisalcrud.faisalmvc.repository;

import com.faisalcrud.faisalmvc.model.entity.Karyawan;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

public interface KaryawanRepository extends JpaRepository<Karyawan, Long> {

    @Modifying
    @Query(value = "UPDATE karywan SET name = ?2 WHERE id = ?1", nativeQuery = true)
    void update(Long id, String name);
}
